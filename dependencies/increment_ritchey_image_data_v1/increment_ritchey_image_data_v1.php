<?php
#Name:Increment Ritchey Image Data v1
#Description:Increment data conformed to Ritchey Image Data v1 by 1 valid increment using content generation techniques. Returns data as a string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Image data must contain a field called "Colour", and it must have an RGB colour code as the value. Any other fields will not be included in the new data. Width is NOT preserved. The width, and height will be made as equal as possible.
#Arguments:'string' is a string containing the data to increment. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('increment_ritchey_image_data_v1') === FALSE){
function increment_ritchey_image_data_v1($string, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@isset($string) === FALSE){
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Break into an array of 1/3 of each pixel's "Colour" field value. Discard all other data, including line endings. Reverse the array so that increment can be done from end to start of image.
		@preg_match_all("/Colour:.*?\./", $string, $select_data, PREG_PATTERN_ORDER);
		$select_data = @implode($select_data[0]);
		$select_data = @str_replace('Colour:', "", $select_data);
		$select_data = @str_replace('.', ",", $select_data);
		$select_data = @trim($select_data);
		$select_data = @explode(',', $select_data);
		$discard = @array_pop($select_data);
		$select_data = @array_reverse($select_data);
		###Increment the first 1/3 between 0-255. When it exceeds 255 it loops back to 0, and the next value is incremented by. If a next doesn't exist create it x3.
		foreach ($select_data as &$value) {
			if ($value < 255){
				$value = $value + 1;
				goto break_increment;
			} else {
				$value = 0;
			}
		}
		unset($value);
		break_increment:
		###Unreverse array
		$select_data = @array_reverse($select_data);
		###Check if entire array is zeros, and if it is add a new section of three zeros so that a new pixel is added.
		$check = FALSE;
		foreach ($select_data as &$value) {
			if ($value > 0){
				$check = TRUE;
			}
		}
		unset($value);
		if ($check === FALSE){
			$select_data[] = '0';
			$select_data[] = '0';
			$select_data[] = '0';
		}
		###Apply required data around new colour value
		$select_data = @array_chunk($select_data, 3);
		foreach ($select_data as &$value) {
			$value = @implode(',', $value);
			$value = "[Colour:{$value}.]";
		}
		unset($value);
		###Determine width, apply it, and convert to string
		$width = @count($select_data);
		$width = @sqrt($width);
		$width = @floor($width);
		$select_data = @array_chunk($select_data, $width);
		foreach ($select_data as &$value) {
			$value = @implode($value);
		}
		unset($value);
		$select_data = @implode("\n", $select_data);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('increment_ritchey_image_data_v1_format_error') === FALSE){
				function increment_ritchey_image_data_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("increment_ritchey_image_data_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $select_data;
	} else {
		return FALSE;
	}
}
}
?>