<?php
#Name:Create Median Ritchey Image Data v1
#Description:Create an image in Ritchey Image Data format for the image half way between two images. Returns data as a string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Image data must contain a field called "Colour", and it must have an RGB colour code as the value. Any other fields will not be included in the new data. Input images must be square, because line-breaks aren't taken into account. This process is extremely slow, and is memory hungry.
#Arguments:'string1' is a string containing the data to increment from. 'string2' is a string containing data to increment to (therefore it must be a larger increment). 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string1:string:required,string2:string:required,display_errors:bool:optional
#Content:
if (function_exists('create_median_ritchey_image_data_v1') === FALSE){
function create_median_ritchey_image_data_v1($string1, $string2, $display_errors = NULL){
	$errors = array();
	if (@isset($string1) === FALSE){
		$errors[] = "string1";
	}
	if (@isset($string2) === FALSE){
		$errors[] = "string2";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Increment from string1 to string2, and count how many increments that is. Divide the total increments by 2, and round to the nearest whole number. Increment from string1 by that amount.]
	if (@empty($errors) === TRUE){
		###If there is a line ending at the end of string1 or string2 remove it.
		$string1 = @rtrim($string1);
		$string2 = @rtrim($string2);
		###Increment from string1 until reaching string2, and count the number of increments it took
		$data = $string1;
		$n = '0';
		while ($data != $string2){
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/increment_ritchey_image_data_v1/increment_ritchey_image_data_v1.php';
			$data = increment_ritchey_image_data_v1($data, TRUE);
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/increment_large_number_v1/increment_large_number_v1.php';
			$n = increment_large_number_v1($n, TRUE);
		}
		###Divide the number of increments by 2 to determine the halfway point
		$location = realpath(dirname(__FILE__));
		require_once $location . '/dependencies/divide_large_number_v1/divide_large_number_v1.php';
		$n2 = divide_large_number_v1($n, '2', TRUE);
		###Increment from string1 by the number of increments needed to reach the halfway point
		$data = $string1;
		$n = $n2;
		unset($n2);
		while ($n != '0'){
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/increment_ritchey_image_data_v1/increment_ritchey_image_data_v1.php';
			$data = increment_ritchey_image_data_v1($data, TRUE);
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/decrement_large_number_v1/decrement_large_number_v1.php';
			$n = decrement_large_number_v1($n, TRUE);
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('create_median_ritchey_image_data_v1_format_error') === FALSE){
				function create_median_ritchey_image_data_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("create_median_ritchey_image_data_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $data;
	} else {
		return FALSE;
	}
}
}
?>